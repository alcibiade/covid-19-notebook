#!/bin/bash

URLS=/tmp/urls_$$

curl 'https://www.who.int/emergencies/diseases/novel-coronavirus-2019/situation-reports' \
    | sed -e 's&<a &\n<a &g' -e 's&</a>&</a>\n&g' \
    | grep 'href=.*-sitrep-.*pdf' \
    | sed -e 's&.*href="&&' -e 's&\?.*&&' -e 's&^&https://www.who.int&' \
    | sort | uniq >${URLS}

for url in $(cat ${URLS})
do
    filename=$(echo $url | sed -e 's&.*/&&')
    test -f $filename || wget $url
done

rm -f ${URLS}
