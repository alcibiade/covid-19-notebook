#!/bin/bash

set -e -x

SCRIPT_DIR=$(dirname $(readlink -f $0))

WHO_BASE=who
REPORTS=reports

# Fetch PDF reports

mkdir -p ${WHO_BASE}/${REPORTS}
pushd .
cd who/reports
${SCRIPT_DIR}/fetch-who-reports.sh
popd

# Update COVID structured datasets from GitHub project

if test ! -d COVID-19
then
    git clone https://github.com/CSSEGISandData/COVID-19.git
else
    cd COVID-19 && git pull && cd ..
fi
